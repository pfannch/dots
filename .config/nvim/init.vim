" Functions {{{
function! s:CommandAlias(abbreviation, expansion) " {{{
  execute 'cabbrev ' . a:abbreviation . ' <c-r>=getcmdpos() == 1 && getcmdtype() == ":" ? "' . a:expansion . '" : "' . a:abbreviation . '"<CR>'
endfunction " }}}
function! s:MkNonExDir(file, buf)
  if empty(getbufvar(a:buf, '&buftype')) && a:file!~#'\v^\w+\:\/'
    let dir=fnamemodify(a:file, ':h')
    if !isdirectory(dir)
      call mkdir(dir, 'p')
    endif
  endif
endfunction
function! <SID>StripTrailingWhitespaces() " {{{
  %s/\s\+$//e
endfunction " }}}
function! <SID>ReturnToLastPosition() " {{{
  if line("'\"") <= line("$")
    normal! g`"
    return 1
  endif
endfunction " }}}
function! s:Completion(mode) " {{{
	let l:line = getline('.')
  let l:substr = strpart(l:line, -1, col('.'))
  let l:substr = matchstr(l:substr, "[^ \t]*$")
  let l:has_period = match(l:substr, '\.') != -1
  let l:has_slash = match(l:substr, '\/') != -1
  if (!l:has_period && !l:has_slash && a:mode == -1)
    return "\<C-x>\<C-n>"
  elseif (!l:has_period && !l:has_slash && a:mode == 1)
    return "\<C-x>\<C-o>"
  elseif (l:has_slash)
    return "\<C-x>\<C-f>"
  elseif (l:has_period)
    return "\<C-x>\<C-o>"
  endif
endfunction " }}}
function! GetBufferList() " {{{
  redir =>buflist
  silent! ls!
  redir END
  return buflist
endfunction " }}}
function! s:BufferDelRespectWindow() " {{{
  if len(filter(range(1, bufnr('$')), 'buflisted(v:val)')) == 1
    lclose
    silent bdelete
  else
    lclose
    bprevious
    silent bdelete #
  endif
endfunction " }}}
function! s:ToggleList(bufname) " {{{
  let l:buflist = GetBufferList()
  for bufnum in map(filter(split(l:buflist, '\n'), 'v:val =~ "'.a:bufname.'"'), 'str2nr(matchstr(v:val, "\\d\\+"))')
    if bufwinnr(bufnum) != -1
      lclose
      call LocationListHandle(0)
      return
    endif
  endfor
  if !len(getloclist(0))
    return
  else
    lopen 10
    call LocationListHandle(1)
  endif
endfunction " }}}
function! s:GrepWrapper(path) " {{{
  if winnr() != 1
    return
  endif

  let l:pattern = substitute(input("/", expand('<cword>')), '"', '\"', '')

  if !empty(l:pattern)
    execute 'silent! lgrep ' . shellescape(l:pattern) . ' ' . a:path . ' /dev/null'
    redraw!
    if len(getloclist(0)) > 0
      lopen 10
      call LocationListHandle(1)
    endif
  else
    redraw!
  endif

endfunction " }}}
function! LocationListHandle(alpha) " {{{
  if a:alpha == 1
    nnoremap <silent> <C-j> :exe "silent! lnext"<CR>
    nnoremap <silent> <C-k> :exe "silent! lprevious"<CR>
    wincmd p
  endif
endfunction " }}}
function! ToggleFoldMethod() " {{{
  let l:foldlist=["indent","syntax","manual","marker"]
  if !exists( "b:foldcurrent" )
    let b:foldcurrent=0
  else
    let b:foldcurrent=index(l:foldlist, &foldmethod)
    let b:foldcurrent=b:foldcurrent+1
    if b:foldcurrent==len(l:foldlist)
      let b:foldcurrent=0
    endif
  endif
  execute "setlocal foldmethod=".get(l:foldlist, b:foldcurrent)
  echom &foldmethod
endfunction " }}}
function! ToggleSpell() " {{{
  let l:langlist=["nospell","de_de","en_us"]
  if !exists( "b:langcurrent" )
    if &spell
      let b:langcurrent=index(l:langlist, &spelllang)
    else
      let b:langcurrent=0
    endif
  endif
  let b:langcurrent=b:langcurrent+1
  if b:langcurrent==len(l:langlist)
    let b:langcurrent=0
  endif
  if b:langcurrent==0
    setlocal nospell
    echom "nospell"
  else
    execute "setlocal spell spelllang=".get(l:langlist, b:langcurrent)
    echom &spelllang
  endif
endfunction " }}}
" }}}
" General {{{

scriptencoding utf-8

augroup vimrc
  autocmd!
augroup END

autocmd vimrc BufWritePost $MYVIMRC nested source $MYVIMRC

autocmd BufEnter * if &buftype == 'terminal' | :startinsert | endif

tnoremap <Esc> <C-\><C-n>

" Enable filetype plugins
filetype plugin indent on

set lazyredraw

set notimeout
set nottimeout

set nobackup
set nowritebackup
set modelines=2

set autoread
set noautowrite

if isdirectory(".git")
  set path=.,**
  set tags=.git/tags
else
  set path=$HOME/\.config/**
  set path+=$HOME/\.local/**
endif

" Completion
set omnifunc=syntaxcomplete#Complete
set complete=.,b,u,t,i
set completeopt=menu

" Searching
set ignorecase
set smartcase
set hlsearch
set incsearch

let &grepprg = "grep -n --recursive"

" Buffer
set hidden

" Swap files
set directory=$XDG_DATA_HOME/nvim/swap//

" Persistent undo
set undofile
set undolevels=1000
set undoreload=10000
set undodir=$XDG_DATA_HOME/nvim/undo

" }}}
" Commands {{{

" Replace a builtin command using cabbrev
command! -nargs=+ CommandAlias call <SID>CommandAlias(<f-args>)

CommandAlias Q q
CommandAlias Q! q!
CommandAlias QA qa
CommandAlias Qa qa
CommandAlias qA qa
CommandAlias QA! qa!
CommandAlias Qa! qa!
CommandAlias qA! qa!
CommandAlias WQ wq
CommandAlias Wq wq
CommandAlias wQ wq

" }}}
" Display {{{

" Interface
syntax on

set termguicolors
let g:lucius_style = "light"
let g:lucius_contrast = "normal"
let g:lucius_contrast_bg = "high"
colorscheme lucius

exe "hi! nCursor" . " guibg=#5fd7d7"
exe "hi! iCursor" . " guibg=#5fd7d7"
exe "hi! TermCursor" . " guifg=#5fd7d7"

" Cursor
set guicursor=n-v-c-sm:nCursor-block,i-ci-ve-r-cr-o:iCursor-hor25

set nocursorline

set fillchars+=vert:\
set wmh=0

" Title
set title
set titlestring=%t%(\ %M%)%(\ (%{expand(\"%:~:h\")})%)%(\ %a%)

" Scrolling
set scrolloff=3
set sidescrolloff=3
set sidescroll=1
set virtualedit+=block
set nostartofline
set display+=lastline

" Folding
set foldenable
set foldmethod=marker

" Completion
set pumheight=10

" Windows
set switchbuf=useopen
set equalalways

" Commandline
set cmdheight=1
set cmdwinheight=7
set shortmess=AatIcso
set noshowmode
set showcmd
set history=1000
set report=0
set wildmenu
set wildcharm=<C-Z>
set wildmode=list,full
set wildignorecase
set wildignore=*.swp,*.bak,*.db
set wildignore+=*~,~*
set wildignore+=*%,%*
set wildignore+=*.png,*.PNG,*.jpg,*.JPG,*.jpeg,*.JPEG,*.gif,*.GIF,*.pdf,*.PDF
set wildignore+=*.zip,*.tar.*
set wildignore+=*/.git/**/*

" Statusline
set laststatus=0

" Automatically open quickfix window after commands that modify the quickfix list
autocmd vimrc QuickFixCmdPost *grep* if len(getqflist()) | copen | endif

" Strips trailing whitespace at the end of lines
autocmd vimrc BufWritePre * :call <SID>StripTrailingWhitespaces()

" Returns to last position when opening files
autocmd vimrc BufReadPost * :call <SID>ReturnToLastPosition()

" }}}
" Filetypes {{{

autocmd vimrc FileType haskell
  \ setlocal ts=4 sts=4 sw=4 tw=79 cc=+1
  \ makeprg=stack
  \ omnifunc=necoghc#omnifunc |
  \ nnoremap <silent><buffer> gmi :GhcModTypeInsert<CR> |
  \ nnoremap <silent><buffer> gms :GhcModSplitFunCase<CR> |
  \ nnoremap <silent><buffer> gmt :GhcModType<CR> |
  \ nnoremap <silent><buffer> gmc :GhcModTypeClear<CR> |
  \ inoreab <buffer> int Int |
  \ inoreab <buffer> integer Integer |
  \ inoreab <buffer> string String |
  \ inoreab <buffer> double Double |
  \ inoreab <buffer> float Float |
  \ inoreab <buffer> bool Bool |
  \ inoreab <buffer> true True |
  \ inoreab <buffer> false False |
  \ inoreab <buffer> maybe Maybe |
  \ inoreab <buffer> just Just |
  \ inoreab <buffer> nothing Nothing |
  \ inoreab <buffer> either Either |
  \ inoreab <buffer> left Left |
  \ inoreab <buffer> right Right |
  \ inoreab <buffer> io IO ()

autocmd vimrc FileType python
  \ setlocal ts=4 sts=4 sw=4 tw=79 cc=+1

autocmd vimrc FileType markdown
  \ setlocal tw=79 cc=+1

autocmd vimrc FileType sh,zsh
  \ setlocal ts=2 sts=2 sw=2 tw=79 cc=+1

autocmd vimrc FileType mail
  \ setlocal tw=72 cc=+1 fo+=aw

autocmd vimrc FileType qf
  \ setlocal nobuflisted

" }}}
" Textformatting {{{

"set formatoptions+=j
set backspace=indent,eol,start
set whichwrap+=<,>,[,],h,l
set iskeyword+=$
"set selection=inclusive

" Indents
set autoindent
set smartindent
"set wrap

" Matching
set matchpairs=(:),{:},[:],<:>
set showmatch
set mat=1

" Tabs
set ts=2 sts=2 sw=2
set shiftround
set expandtab
set smarttab

" }}}
" Keybindings {{{

" Completion
inoremap <expr> <C-j> pumvisible() ? "\<C-n>" : <SID>Completion(-1)
inoremap <expr> <C-k> pumvisible() ? "\<C-p>" : <SID>Completion(1)
inoremap <expr> <CR> pumvisible() ? "\<C-y>" : "\<CR>"
inoremap <expr> <C-c> pumvisible() ? "\<C-e>" : "\<C-c>"

" Command-Line
cnoremap <expr> <C-j> wildmenumode() ? "\<Down>" : "\<C-n>"
cnoremap <expr> <C-k> wildmenumode() ? "\<Up>" : "\<C-p>"

" Buffers
nnoremap j gj
nnoremap k gk
nnoremap 0 g0
nnoremap ^ g^
nnoremap $ g$
nnoremap Y y$

" Tab / Shift-Tab - forward/backward indenting
xnoremap <silent> <Tab> >gv
xnoremap <silent> <S-Tab> <gv

" space - toggle folds
nnoremap <silent> <space> :<C-u>execute "normal! " . v:count1 . "za"<CR>
vnoremap <space> zf

" Ctrl-c - remove highlighted search
nnoremap <silent> <C-c> :nohlsearch<CR><C-l>

" Tab - focus last buffer
nnoremap <silent> <Tab> :bnext<CR>
nnoremap <silent> <S-Tab> :bnext<CR>

" M-w - write buffer
nnoremap <silent> <M-w> :silent update<CR>

" M-d - delete the current buffer without closing the window
nnoremap <silent> <M-d> :call <SID>BufferDelRespectWindow()<CR>

" M-D - delete the current buffer and close the corresponding window
nnoremap <silent> <M-D> :lclose<CR>:silent bdelete<CR>

" M-l - toggle location list
nnoremap <silent> <M-l> :call <SID>ToggleList('Location List')<CR>

" M-g and M-G - grep the current file or path
nnoremap <silent> <M-g> :call <SID>GrepWrapper('%')<CR>
nnoremap <silent> <M-G> :call <SID>GrepWrapper('.')<CR>

" M-F - toggle fold method
nnoremap <silent> <M-F> :call ToggleFoldMethod()<CR>

" M-S - toggle spell check
nnoremap <silent> <M-S> :call ToggleSpell()<CR>

" M-N - toggle relativenumber
nnoremap <silent> <M-N> :set relativenumber!<CR>

" M-T - display directory-tree
nnoremap <silent> <M-T> :Lexplore<CR>
" }}}
" Plugins {{{
" Netrw
let g:netrw_silent = 1
let g:netrw_banner = 0
let g:netrw_liststyle = 3
let g:netrw_browse_split = 4
let g:netrw_altv = 1
let g:netrw_winsize = 14
let g:netrw_sort_by = "time"
let g:netrw_sort_direction = "reverse"

" Ctrlp
let g:ctrlp_map = ''
let g:ctrlp_cmd = ''
let g:ctrlp_match_window = 'bottom,orderbtt,min:1,max:10,results:10'
let g:ctrlp_cache_dir = $XDG_CACHE_DIR.'/ctrlp'
let g:ctrlp_show_hidden = 0
let g:ctrlp_open_new_file = 'r'
let g:ctrlp_open_multiple_files = 'ri'
let g:ctrlp_arg_map = 0
let g:ctrlp_follow_symlinks = 1
let g:ctrlp_types = ['fil', 'mru', 'buf']
let g:ctrlp_extensions = []
let g:ctrlp_line_prefix = '> '
let g:ctrlp_brief_prompt = 0
let g:ctrlp_prompt_mappings = {
  \ 'PrtBS()':              ['<bs>', '<c-]>'],
  \ 'PrtDelete()':          ['<del>'],
  \ 'PrtDeleteWord()':      ['<c-w>'],
  \ 'PrtClear()':           ['<c-u>'],
  \ 'PrtSelectMove("j")':   ['<c-j>'],
  \ 'PrtSelectMove("k")':   ['<c-k>'],
  \ 'PrtSelectMove("t")':   ['<Home>', '<kHome>'],
  \ 'PrtSelectMove("b")':   ['<End>', '<kEnd>'],
  \ 'PrtSelectMove("u")':   ['<PageUp>', '<kPageUp>'],
  \ 'PrtSelectMove("d")':   ['<PageDown>', '<kPageDown>'],
  \ 'PrtHistory(-1)':       ['<c-n>'],
  \ 'PrtHistory(1)':        ['<c-p>'],
  \ 'AcceptSelection("e")': ['<cr>', '<2-LeftMouse>'],
  \ 'AcceptSelection("h")': ['<c-x>', '<c-cr>', '<c-s>'],
  \ 'AcceptSelection("t")': ['<c-t>'],
  \ 'AcceptSelection("v")': ['<c-v>', '<RightMouse>'],
  \ 'ToggleFocus()':        ['<s-tab>'],
  \ 'ToggleRegex()':        ['<c-r>'],
  \ 'ToggleByFname()':      ['<c-d>'],
  \ 'ToggleType(1)':        ['<c-f>', '<c-up>'],
  \ 'ToggleType(-1)':       ['<c-b>', '<c-down>'],
  \ 'PrtExpandDir()':       ['<tab>'],
  \ 'PrtInsert("c")':       ['<MiddleMouse>', '<insert>'],
  \ 'PrtInsert()':          ['<c-\>'],
  \ 'PrtCurStart()':        ['<c-a>'],
  \ 'PrtCurEnd()':          ['<c-e>'],
  \ 'PrtCurLeft()':         ['<c-h>', '<c-^>'],
  \ 'PrtCurRight()':        ['<c-l>'],
  \ 'PrtClearCache()':      ['<F5>'],
  \ 'PrtDeleteEnt()':       ['<F7>'],
  \ 'CreateNewFile()':      ['<c-y>'],
  \ 'MarkToOpen()':         ['<c-z>'],
  \ 'OpenMulti()':          ['<c-o>'],
  \ 'PrtExit()':            ['<esc>', '<c-c>', '<c-g>'],
  \ }

nnoremap <silent> <M-e> :CtrlP<CR>
nnoremap <silent> <M-E> :call <SID>BufferDelRespectWindow()<CR>:CtrlP<CR>
nnoremap <silent> <M-u> :CtrlPMRUFiles<CR>
nnoremap <silent> <M-t> :CtrlPTag<CR>
nnoremap <silent> <M-b> :CtrlPBuffer<CR>
nnoremap <silent> gb :CtrlPBuffer<CR>
" }}}
