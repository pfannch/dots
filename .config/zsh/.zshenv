export ICEAUTHORITY=$XDG_RUNTIME_DIR/ICEauthority
export XAUTHORITY=$XDG_RUNTIME_DIR/Xauthority
export XINITRC=$XDG_CONFIG_HOME/xorg/xinitrc

export XDG_CONFIG_HOME=$HOME/.config
export XDG_CACHE_HOME=$HOME/.cache
export XDG_DATA_HOME=$HOME/.local/share

export EDITOR='nvim'
export VISUAL=$EDITOR
export PAGER='less -x4'
export MANPAGER=$PAGER
export BROWSER='qutebrowser'

export GNUPGHOME=$XDG_CONFIG_HOME/gnupg
export GPGKEY="$(gpg --list-secret-keys --keyid-format short | grep 'sec' | egrep '[0-9A-Z]{8}' -o)"

export PASSWORD_STORE_DIR=$XDG_DATA_HOME/pass
export PASSWORD_STORE_GIT=$XDG_DATA_HOME/pass
export PASSWORD_STORE_X_SELECTION=primary
export PASSWORD_STORE_CLIP_TIME=45

export LESSHISTFILE=-
export LESS_TERMCAP_mb=$(tput bold; tput setaf 2)
export LESS_TERMCAP_md=$(tput bold; tput setaf 6)
export LESS_TERMCAP_me=$(tput sgr0)
export LESS_TERMCAP_so=$(tput setaf 1)
export LESS_TERMCAP_se=$(tput rmso; tput sgr0)
export LESS_TERMCAP_us=$(tput smul; tput bold; tput setaf 1)
export LESS_TERMCAP_ue=$(tput rmul; tput sgr0)
export LESS_TERMCAP_mr=$(tput rev)
export LESS_TERMCAP_mh=$(tput dim)
export LESS_TERMCAP_ZN=$(tput ssubm)
export LESS_TERMCAP_ZV=$(tput rsubm)
export LESS_TERMCAP_ZO=$(tput ssupm)
export LESS_TERMCAP_ZW=$(tput rsupm)
