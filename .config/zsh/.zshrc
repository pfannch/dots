# Aliases {{{

alias ls='ls -lht -F --color=auto --time-style=+%m-%d" "%H:%M'
alias watch='watch --color'
alias grep='grep --color=auto'
alias mkdir='mkdir -p'
alias du='du -ch'
alias df='df -Th --total'
alias ln='ln -i'
alias cp='cp -i'
alias mv='mv -i'
alias rm='rm -i'
alias chown='chown -c'
alias chmod='chmod -c'
alias chgrp='chgrp -c'
alias free='free -ht'
alias less='less -x4'
alias sudo='sudo '

alias l='ls'
alias la='l -A'
alias d='dirs -v'
alias m='man'
alias g='git'
alias t='task'
alias up='uptime -p'
alias nmutt='neomutt'

alias s="g --git-dir=$HOME/.setup --work-tree=$HOME"

alias pg='pgrep'
alias pk='pkill'

alias pm='pacman'
alias sc='systemctl'
alias scu='systemctl --user'
alias jc='journalctl'
alias jcu='journalctl --user-unit'

alias ssh='ssh -q -C4'

alias -g H='| head'
alias -g T='| tail'
alias -g G='| grep'
alias -g L='| less'
alias -g W='| wc'

alias -g ...='../..'
alias -g ....='../../..'
alias -g .....='../../../..'

alias -g DN='/dev/null'

alias -s hs=$EDITOR
alias -s txt=$EDITOR
alias -s csv=$EDITOR
alias -s htm=$BRWSER
alias -s html=$BROWSER
alias -s pdf='zathura'
alias -s zip='unzip'
alias -s tar='tar -xvf'
alias -s gz='tar -xzvf'
alias -s bz2='tar -xjvf'

# }}}
# Functions {{{
precmd() # {{{
{
} # }}}
chpwd() # {{{
{
  emulate -L zsh
  if git rev-parse --git-dir > /dev/null 2>&1; then
    ls; g s
  else
    ls
  fi
} # }}}
zle-line-init() # {{{
{
  zle -K viins
  echo -ne "\e[4 q"
} # }}}
zle-keymap-select() # {{{
{
  if [ $KEYMAP = vicmd ];
  then
    echo -ne "\e[2 q"
  else
    echo -ne "\e[4 q"
  fi
} # }}}
mo() # {{{
{
  man -P "less -p \"^ +$2\"" $1
} # }}}
me() # {{{
{
  man -P "less -p \"^EXAMPLES?\"" $1
} # }}}
cl() # {{{
{
  cd "${1-$HOME}" && ls
} # }}}
mkd() # {{{
{
  mkdir -p "$@" && cd "$_"
} # }}}
ff() # {{{
{
  find . -type f -iname "$@" 2> >(grep -v 'Permission denied\|Input/output error' >&2)
} # }}}
fl() # {{{
{
  find . -type l -iname "$@" 2> >(grep -v 'Permission denied\|Input/output error' >&2)
} # }}}
fd() # {{{
{
  find . -type d -iname "$@" 2> >(grep -v 'Permission denied\|Input/output error' >&2)
} # }}}
fp() # {{{
{
  find . -type p -iname "$@" 2> >(grep -v 'Permission denied\|Input/output error' >&2)
} # }}}
fs() # {{{
{
  find . -type s -iname "$@" 2> >(grep -v 'Permission denied\|Input/output error' >&2)
} # }}}
update() # {{{
{
  case "$1" in
    pkgs|all)
      sudo pacman -Syu
      ;|
    keys|all)
      sudo pacman-key --refresh-keys
      ;|
    mirrors|all)
      sudo reflector --country 'Germany' --latest 200 --age 24 --sort rate \
        --save /etc/pacman.d/mirrorlist
      ;|
    nvim|all)
      nvim --headless -c "call dein#update()" -c "qall"
      ;;
    "")
      echo "usage: $(basename "$0") <pkgs|keys|mirrors|nvim|all>"
      ;;
  esac
} # }}}
clean() # {{{
{
  case "$1" in
    cache|all)
      sudo pacman -Scc
      ;|
    deps|all)
      sudo pacman -Rns $(pacman -Qtdq)
      ;;
    dropped|all)
      sudo pacman -Rns $(pacman -Qm)
      ;;
    "")
      echo "usage: $(basename "$0") <cache|deps|dropped|all>"
      ;;
  esac
} # }}}
run_with_sudo() # {{{
{
  LBUFFER="sudo $LBUFFER"
} # }}}
backward_delete_to_slash() # {{{
{
  local WORDCHARS=${WORDCHARS//\//}
  zle .backward-delete-word
} # }}}
ctrl_z() # {{{
{
  if [[ $#BUFFER -eq 0 ]]; then
    if [[ -n $(jobs) ]]; then
      BUFFER="fg"
      zle accept-line
      zle redisplay
    else
      true
    fi
  else
    zle push-input
  fi
} # }}}
expand_or_complete_with_dots() # {{{
{
  echo -n "..."
  zle expand-or-complete
  zle redisplay
} # }}}
parse_git_state() # {{{
{
  local GIT_PROMPT_AHEAD="+"
  local GIT_PROMPT_BEHIND="-"
  local GIT_PROMPT_MERGING="x"
  local GIT_PROMPT_SEPARATOR=" "
  local GIT_STATE=""
  local ref=$(git symbolic-ref -q HEAD || git name-rev --name-only --no-undefined --always HEAD) 2> /dev/null

  if [[ $(git status --porcelain --ignore-submodules -unormal) == "" ]]
  then
    GIT_STATE=$GIT_STATE"%F{green}${ref#(refs/heads/|tags/)}%f"
  else
    GIT_STATE=$GIT_STATE"%F{red}${ref#(refs/heads/|tags/)}%f"
  fi

  local NUM_AHEAD="$(git log --oneline @{u}.. 2> /dev/null | wc -l | tr -d ' ')"
  if [ "$NUM_AHEAD" -gt 0 ]; then
    GIT_STATE=$GIT_STATE$GIT_PROMPT_SEPARATOR$GIT_PROMPT_AHEAD$NUM_AHEAD
  fi

  local NUM_BEHIND="$(git log --oneline ..@{u} 2> /dev/null | wc -l | tr -d ' ')"
  if [ "$NUM_BEHIND" -gt 0 ]; then
    GIT_STATE=$GIT_STATE$GIT_PROMPT_SEPARATOR$GIT_PROMPT_BEHIND$NUM_BEHIND
  fi

  local GIT_DIR="$(git rev-parse --git-dir 2> /dev/null)"
  if [ -n $GIT_DIR ] && test -r $GIT_DIR/MERGE_HEAD; then
    GIT_STATE=$GIT_STATE$GIT_PROMPT_SEPARATOR$GIT_PROMPT_MERGING
  fi

  echo " $GIT_STATE"

} # }}}
git_prompt_string() # {{{
{
  if git rev-parse --git-dir > /dev/null 2>&1; then
    parse_git_state
  fi
} # }}}
ssh_prompt_string() # {{{
{
  local SSH_STATE=""
  [ -n "$SSH_CONNECTION" ] && SSH_STATE="%n@%m"

  echo " $SSH_STATE"

} # }}}
# }}}
# General {{{
color0="#444444"
color1="#af0000"
color2="#008700"
color3="#af5f00"
color4="#005faf"
color5="#870087"
color6="#5fd7d7"
color7="#ffffff"
color8="#444444"
color9="#af0000"
color10="#008700"
color11="#af5f00"
color12="#005faf"
color13="#870087"
color14="#5fd7d7"
color15="#ffffff"
# }}}
# Path {{{

typeset -U path

path=($HOME/.local/bin "$path[@]")

# }}}
# Options {{{

ttyctl -f

HISTFILE=$XDG_DATA_HOME/zsh/history
HISTSIZE=100000
SAVEHIST=$HISTSIZE
HISTORY_IGNORE="(cd|ls|exit|pwd|date|clear|man *|* --help)"

setopt hist_verify
setopt inc_append_history
setopt hist_ignore_all_dups
setopt hist_reduce_blanks
setopt no_hist_beep
setopt share_history

setopt ignore_eof
setopt no_hup
setopt no_beep
setopt no_list_beep
setopt no_flow_control
setopt notify
setopt auto_cd
setopt auto_pushd
setopt pushd_ignore_dups
setopt pushd_minus
setopt pushd_silent
setopt pushd_to_home
setopt extended_glob
setopt prompt_subst
setopt rm_star_silent
setopt print_exit_value
setopt transient_rprompt
setopt correct_all

# }}}
# Prompt {{{

autoload -Uz colors && colors

PROMPT='%3~$(git_prompt_string)$(ssh_prompt_string)
:'

RPROMPT='%(1j.%j.)'

# }}}
# Completion {{{

eval $(dircolors $XDG_DATA_HOME/zsh/dircolors_dark)
autoload -Uz compinit
compinit -d $XDG_DATA_HOME/zsh/zcompdump

zstyle ':completion:*' rehash on
zstyle ':completion:*' use-cache on
zstyle ':completion:*' cache-path $XDG_DATA_HOME/zsh/zcompcache

zstyle ':completion:*' completer _complete _approximate
zstyle ':completion:*:approximate:*' max-errors 0 numeric
zstyle ':completion:*' menu select=1
zstyle ':completion:*:default' list-colors ${(s.:.)LS_COLORS}
zstyle ':completion:*' list-colors ''
zstyle ':completion:*' group-name ''
zstyle ':completion:*:default' list-prompt '%p'

zstyle ':completion:*:processes' command "ps -u $(whoami) -o pid,user,comm,command"

zstyle ':completion:*:manuals' separate-sections on

zstyle ':completion:*' special-dirs on
zstyle ':completion:*:cd:*' ignore-parents parent pwd

zstyle ':completion:*:tar:*' files '*.tar.*'

zstyle ':completion:*:nvim:*' ignored-patterns '*.(hi|o|swp|bak|zip|tar|tar.*|png|PNG|jpg|JPG|jpeg|JPEG|gif|GIF|pdf|PDF)'

# }}}
# Keybindings {{{

KEYTIMEOUT=1

bindkey -v

zle -N zle-keymap-select
zle -N zle-line-init

zle -N run_with_sudo
bindkey '^Xs' run_with_sudo

zle -N backward_delete_to_slash
bindkey '^q' backward_delete_to_slash

zle -N ctrl_z
bindkey '^z' ctrl_z

zle -N expand_or_complete_with_dots
bindkey '^i' expand_or_complete_with_dots

bindkey '^[[Z' reverse-menu-complete

bindkey -M viins '^a' beginning-of-line
bindkey -M viins '^e' end-of-line
bindkey -M viins '^b' backward-char
bindkey -M viins '^f' forward-char
bindkey -M viins '^r' history-incremental-pattern-search-backward
bindkey -M viins '^s' history-incremental-pattern-search-forward
bindkey -M viins '^n' history-search-forward
bindkey -M viins '^p' history-search-backward
bindkey -M viins '^h' backward-delete-char
bindkey -M viins '^?' backward-delete-char
bindkey -M viins '^d' delete-char-or-list
bindkey -M viins '^t' transpose-chars
bindkey -M viins '^w' backward-kill-word
bindkey -M viins '^u' kill-whole-line
bindkey -M viins '^y' yank
bindkey -M viins '^_' undo

bindkey -M vicmd '?' vi-history-search-backward
bindkey -M vicmd '/' vi-history-search-forward

bindkey -M vicmd '^a' beginning-of-line
bindkey -M vicmd '^e' end-of-line
bindkey -M vicmd '^b' backward-char
bindkey -M vicmd '^f' forward-char
bindkey -M vicmd '^h' backward-delete-char
bindkey -M vicmd '^?' backward-delete-char
bindkey -M vicmd '^d' delete-char-or-list
bindkey -M vicmd '^t' transpose-chars
bindkey -M vicmd '^w' backward-kill-word
bindkey -M vicmd '^u' kill-whole-line
bindkey -M vicmd '^y' yank
bindkey -M vicmd '^_' undo

# }}}
