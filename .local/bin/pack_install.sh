#!/usr/bin/env bash

set_group() {
  name=$1
  flag=$2
  path="$XDG_DATA_HOME/nvim/site/pack/$name/$flag"

  mkdir -p "$path"
  cd "$path" || exit
}

plugin() {
  url=$1
  name=$(basename "$url" .git)
  if [[ -d "$name" ]]; then
    cd "$name" || exit
    result=$(git pull --force)
    echo "$name: $result"
  else
    echo "$name: Installing..."
    git clone -q "$url"
  fi
}

(
set_group colorschemes opt
plugin https://github.com/jonathanfilip/vim-lucius &
wait
) &

(
set_group core start
plugin https://github.com/ctrlpvim/ctrlp.vim &
wait
) &

wait
